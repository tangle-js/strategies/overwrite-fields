# @tangle/overwrite-fields

This strategy can be used to handle transformations which are objects which have arbitrarily set
keys and values (potentially conforming to some pattern):

```js
const T = {
  '@mix': 'mix irving',
  '@colin': 'Colin Pilborow'
}
```

Using concat, subfields can be given new values and additional subfields can be added.
```
overwrite.concat(
  {
    '@mix': 'mixmix',
    '@colin': 'Colin Pilborow'
  },
  {
    '@mix': 'Mix Irving',
    '@cherese': 'Cherese Epinu'
  }
)
// => {
//   '@mix': 'Mix Irving',        // overwrite
//   '@colin': 'Colin Pilborow'
//   '@cherese': 'Cherese Epinu'
// }
```

The internal implementation means that once a subfield is set, it cannot be unset.
If you must erase a field you may want to allow values such as `null` and then choose to ignore that.
This strategy is noncommutative, so it conflicts if the same subfield is set to
different values in different branches. A merge is valid if it overwrites all conflicting subfields.
see tests for examples.

## API

### `Overwrite(opts) => overwrite`

`opts` *Object* (optional) can have properties:
- `opts.keyPattern` *String*
    - add a JSON-schema string pattern
    - default: `'^.+$'`
- `opts.valueSchema` *Object*
    - add a JSON-schema to validate the values which are allowed to be passed in
    - default: `{ type: 'string' }`

### `overwrite.schema`
### `overwrite.isValid`
### `overwrite.identity() => I`
### `overwrite.concat(R, S) => T`
### `overwrite.mapFromInput(input, currentTips) => T`
### `overwrite.mapToOutput(T) => state`

### `overwrite.isConflict(graph, nodeIds, field) => Boolean`
where:
- `graph` is a `@tangle/graph` instance
- `nodeIds` is an Array of nodeIds you're wanting to check for conflict
- `field` *String* contains the the data fields `node.data[field]` you're checking for conflicts

### `overwrite.isValidMerge(graph, mergeNode, field) => Boolean`

where:
- `graph` is a `@tangle/graph` instance
- `mergeNode` is the proposed merge-node
- `field` *String* contains the the data fields `node.data[field]` you're checking for merge validity

### `overwrite.merge(graph, mergeNode, field) => T`

similar to `isValidMerge`, but return a transformation, `T`
If it cannot, an error is thrown!
