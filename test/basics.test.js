const test = require('tape')
const { isValid: isValidStrategy } = require('@tangle/strategy')
const Strategy = require('../')

test('Strategy.isValid', t => {
  t.true(isValidStrategy(Strategy()), 'is a valid strategy according to @tangle/strategy')
  if (isValidStrategy.error) console.log(isValidStrategy.error)

  t.end()
})

/* schema/ isValid */
test('schema', t => {
  const { isValid, identity } = Strategy({
    valueSchema: { type: 'array' }
  })

  const Ts = [
    {
      '%profileD': ['%profileE', '%profileB']
    },
    {
      '%profileD': ['%profileE', '%profileB'],
      '%profileX': ['%profileA']
    },
    {
      '%profileD': [{ happy: true, name: 'dave' }]
    },
    {},
    identity()
  ]

  Ts.forEach(T => {
    t.true(isValid(T), `isValid : ${JSON.stringify(T)}`)
  })

  const notTs = [
    'dog',
    { content: 'dog' },
    { set: null }
  ]
  notTs.forEach(T => {
    t.false(isValid(T), `!isValid : ${JSON.stringify(T)}`)
  })

  t.end()
})

test('mapFromInput', t => {
  const { mapFromInput } = Strategy()

  t.deepEqual(
    mapFromInput({ A: 'B' }),
    { A: 'B' }
  )

  t.throws(() => mapFromInput('dog'))

  t.end()
})

test('mapToOutput', t => {
  const { mapToOutput } = Strategy()

  t.deepEqual(
    mapToOutput({ D: ['E', 'A'] }),
    { D: ['E', 'A'] }
  )

  t.end()
})

test('concat, identity + associativity', t => {
  const { concat, identity } = Strategy({
    valueSchema: { type: 'array' }
  })

  const T = {
    D: ['E', 'A']
  }
  t.deepEqual(
    concat(T, identity()),
    T,
    'identity (right)'
  )
  t.deepEqual(
    concat(identity(), T),
    T,
    'identity (left)'
  )

  t.deepEqual(
    concat(
      {
        D: ['E', 'A'],
        X: ['Y']
      },
      {
        D: ['A', 'E']
      }
    ),
    { // expected
      D: ['A', 'E'], // second one wins on this field
      X: ['Y']
    },
    'concat'
  )

  const A = { B: ['D'] }
  const B = { C: ['A'] }
  const C = { C: ['F'] }

  t.deepEqual(
    concat(
      A,
      concat(B, C)
    ),
    concat(
      concat(A, B),
      C
    ),
    'associativity'
  )
  t.end()
})
