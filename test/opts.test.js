const test = require('tape')
const Strategy = require('../')

test('opts.keyPattern', t => {
  const { isValid } = Strategy({
    keyPattern: '^@[a-z]+$'
  })

  const Ts = [
    { '@dog': 'thing' },
    { '@dog': 'thing', '@cat': 'other' },
    { '@dog': 'fido', '@cat': 'nala' }
  ]

  Ts.forEach(T => {
    const result = isValid(T)
    t.true(result, `isValid : ${JSON.stringify(T)}`)
    if (!result) console.log('error:', isValid.errors)
  })

  const notTs = [
    { dog: 'thing' },
    { dog: 'thing', '@cat': 'other' },
    { '@dog party': 'thing' },
    { '@dog6': 'thing' }
  ]
  notTs.forEach(T => {
    t.false(isValid(T), `!isValid : ${JSON.stringify(T)}`)
  })

  t.end()
})

test('opts.valueSchema)', t => {
  const { isValid } = Strategy({
    valueSchema: {
      type: 'array',
      items: {
        type: 'string',
        pattern: '^@\\w+$'
      },
      minimum: 1
    }
  })

  const Ts = [
    { '@dog': ['@thing'] },
    { '@dog': ['@thing', '@other'] }
  ]

  Ts.forEach(T => {
    t.true(isValid(T), `isValid : ${JSON.stringify(T)}`)
  })

  const notTs = [
    { '@dog': [true] },
    { '@dog': ['@thing', 8] },
    { '@dog': 'yes!' },
    { '@dog': { happy: 'yes!' } }
  ]
  notTs.forEach(T => {
    t.false(isValid(T), `!isValid : ${JSON.stringify(T)}`)
  })

  t.end()
})
